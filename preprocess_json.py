'''
Preprocess book review dataset
'reviews_Books.json' from  http://jmcauley.ucsd.edu/data/amazon/
'''

from collections import defaultdict
g = open('book_reviews.txt', 'w')
def parse_data(path):
    g = open(path, 'r')
    for l in g:
        yield eval(l)

reviews = defaultdict(list)
cur_path = 'reviews_Books.json'
a = parse_data(cur_path)
count = 0
for review in a:
    productID = review['asin']
    review_score = review['overall']
    review_text = review['reviewText']
    reviews[productID].append((review_score, review_text))
    count += 1
    if count%10000 == 0:
        print(count)

#end_of_doc = 'vdvinh_end_doc_dummy ' * 20

count = 0
parts_keys = []
products = reviews.keys()
n = len(products)
a = range(0, n, n/10)
a[-1] = n - 1
for i in range(len(a) - 1):
    parts_keys.append(products[a[i]:a[i+1]])
    
parts = []
for keys in parts_keys:
    parts.append(defaultdict(list))
    for key in keys:
        #reviews[key].sort(key=lambda tup: tup[0])
        count += 1
        if count%1 == 0:
            print('sorted' + str(count))
        parts[-1][key] = reviews[key]

import pickle
for i in range(len(parts)):
    pickle.dump(parts[i], open('part_' + str(i), 'w'), protocol=-1)

g.close()