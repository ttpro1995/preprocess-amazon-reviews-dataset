import pickle
from collections import defaultdict
part_name = 'part_9'
part = pickle.load(open(part_name, 'r'))
count = 0
for product_id in part.keys():
    part[product_id].sort(key=lambda tup: tup[0])
    count += 1
    if count%10 == 0:
        print (count)

count = 0
g = open(part_name + '.txt','w')
for product_id in part.keys():
    for _, content in part[product_id]:
        g.write(content + '\n')
    count += 1
    if count%10 == 0:
        print ('product: ' + str(count))
g.close()