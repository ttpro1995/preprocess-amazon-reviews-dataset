# join part of file
list_file = []
for i in range(6):
    list_file.append('output_' + str(i))

target = 'preprocessed.txt'
g = open(target, 'w')
for file in list_file:
    f = open(file, 'r')
    print('Reading ' + file)
    content = f.read()
    print('Done reading ' + file)
    print('Writing ' + file)
    g.write(content + '\n')
    f.close()
    print('Done writing ' + file)
g.close()

