import random
f = open('movies.txt', 'r')
count = 0

cur_productId = None
cur_score = None

moviews = list()
for line in f:
    if line == '\n':
        continue
    a = line.split(':')
    if len(a) < 2:
        continue
    title = a[0]
    tag = title.split('/')[1]
    if tag == 'text':
        count += 1
        content = " : ".join(a[1:])
        moviews.append(content)
        if count % 10000 == 0:
            print('movies: ' + str(count))

random.shuffle(moviews)
m = len(moviews)
part_0_movie = moviews[0:m/3]
part_1_movie = moviews[m/3:2*m/3]
part_2_movie = moviews[2*m/3:]


import pickle
pickle.dump(part_0_movie, open('part_0_movie', 'w'), protocol=2)
pickle.dump(part_1_movie, open('part_1_movie', 'w'), protocol=2)
pickle.dump(part_2_movie, open('part_2_movie', 'w'), protocol=2)