# Preprocess Amazon review

### need two dataset:
- Amazon reviews (movies.txt.gz	) at https://snap.stanford.edu/data/web-Movies.html
- Books reviews (reviews_Books.json.gz) at http://jmcauley.ucsd.edu/data/amazon/

### preprocess_json.py
Preprocess new data format from http://jmcauley.ucsd.edu/data/amazon/
This file preprocess on Book reviews reviews_books.json
Output book_reviews.txt


### preprocess.py
Preprocess old data format
Use to preprocess data from https://snap.stanford.edu/data/web-Movies.html
Output review.txt


### Usage
Download dataset from https://snap.stanford.edu/data/web-Movies.html and http://jmcauley.ucsd.edu/data/amazon/

Unzip movies.txt and reviews_Books.json in same folder with source code

Run

```
python preprocess.py
python preprocess_json.py
```

Join output book_reviews.txt and review.txt into one text file preprocessed.txt

On Linux, command to join two file are

```
cat file1.txt file2.txt >file.txt
```



