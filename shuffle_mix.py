import random
import pickle
book_reviews = pickle.load(open('part_2_book', 'r'))
movie_reviews = pickle.load(open('part_2_movie', 'r'))

book_reviews.extend(movie_reviews)
random.shuffle(book_reviews)

n = len(book_reviews)
first_part = book_reviews[0:n/2]
second_part = book_reviews[n/2:]

first_txt = '\n'.join(first_part)
second_txt = '\n'.join(second_part)

first_shuffle = open('shuffle_4', 'w')
second_shuffle = open('shuffle_5', 'w')

first_shuffle.write(first_txt)
second_shuffle.write(second_txt)

first_shuffle.close()
second_shuffle.close()
