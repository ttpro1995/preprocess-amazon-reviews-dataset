from collections import defaultdict

f = open('movies.txt', 'r')
g = open('reviews.txt', 'w')
count = 0

cur_productId = None
cur_score = None

reviews = defaultdict(list)

for line in f:
    if line == '\n':
        continue
    a = line.split(':')
    if len(a) < 2:
        continue
    title = a[0]
    tag = title.split('/')[1]
    if tag == 'productId':
        cur_productId = tag
    elif tag == 'score':
        cur_score = tag
    elif tag == 'text':
        count += 1
        content = " : ".join(a[1:])
        reviews[cur_productId].append((cur_score, content))
        print(count)

for productId in reviews.keys():
    reviews[productId].sort(key=lambda tup: tup[0])
    for _, content in reviews[productId]:
        g.write(content + '\n')

f.close()
g.close()
