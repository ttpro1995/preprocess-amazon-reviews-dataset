import random

def parse_data(path):
    g = open(path, 'r')
    for l in g:
        yield eval(l)


reviews = list()
cur_path = 'reviews_Books.json'
a = parse_data(cur_path)
count = 0
for review in a:
    review_text = review['reviewText']
    reviews.append(review_text)
    count += 1
    if count % 10000 == 0:
        print('book: ' + str(count))

random.shuffle(reviews)
n = len(reviews)
part_0_book = reviews[0:n/3]
part_1_book = reviews[n/3:2*n/3]
part_2_book = reviews[2*n/3:]

import pickle
pickle.dump(part_0_book, open('part_0_book', 'w'), protocol=2)
pickle.dump(part_1_book, open('part_1_book', 'w'), protocol=2)
pickle.dump(part_2_book, open('part_2_book', 'w'), protocol=2)



